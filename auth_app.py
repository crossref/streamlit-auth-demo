import streamlit as st
import cr_auth
import app

# Main

if "debug" not in st.session_state:
    st.session_state.debug = False

if cr_auth.first_login():
    # set user and password state variables to none
    cr_auth.init_login()

if cr_auth.all_credenitials_provided():
    # if we have all our credentials, run the app
    app.run()
elif not cr_auth.authenticated():
    # if we have not authenticated, then show login screen
    cr_auth.show_login()  
else:
    # if we have authenticated but not yet seleted a role, show role selector
    cr_auth.show_role_selector()
