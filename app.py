import streamlit as st
import cr_auth

def action_selected():
    st.write(st.session_state.action)

def show_sidebar():
    with st.sidebar:
        cr_auth.show_logged_in_as()
        st.subheader("App contols")
        st.selectbox(label="Action",options=['Foo','Bar','Baz'], on_change=action_selected, key="action")

def show_current_credentials(): 
    st.write(f"User is {st.session_state.user_name}")
    st.write(f"Role is {st.session_state.role}")

def show_splashpage():
    st.header("Application splash page")

def show_results():
    st.header("The Application Main Page")
    show_current_credentials()


### This is where the main app logic starts
def run():


    show_sidebar()

    if "show_splash" not in st.session_state:
        # If this is the first run of the session, we want to show the splash page
        st.session_state.show_splash = True

    

    if st.session_state.show_splash:
        show_splashpage()
        st.session_state.show_splash = False
    else:
        show_results()
        